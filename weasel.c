#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

const char* letter_pot = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
size_t letter_pot_length = strlen(letter_pot);

const char* target = "METHINKS IT IS LIKE A WEASEL";
size_t species_length = strlen(target);

void clone_and_pick_new_species(char* species)
{
  int clones = 100;
  int mutate_chance = 5;
  char* species_pot = (char *)malloc(clones*species_length);

  // generate clones
  {
    for(int c = 0; c < clones; c++)
    {
      int pot_offset = c*species_length;
      for(int i = 0; i < species_length; i++)
      {
        species_pot[pot_offset + i] = species[i];

        if ( (rand() % 100) <= mutate_chance ) {
          int mutate_index = rand() % species_length;
          int pick = rand() % letter_pot_length;
          species_pot[pot_offset + mutate_index] = letter_pot[pick];
        }
      }
    }
    species_pot[clones*species_length] = '\0';
  }


  // pick best clone
  {
    int best_clone = 0;
    int best_similarities = 0;
    for(int c = 0; c < clones; c++)
    {
      int pot_offset = c*species_length;

      int similarities = 0;
      for(int i = 0; i < species_length; i++)
      {
        char species_letter = species_pot[pot_offset + i];

        if (species_letter == target[i]) {
          similarities++;
        }

        if (similarities > best_similarities) {
          best_similarities = similarities;
          best_clone = c;
        }
      }
    }

    char* best_clone_buf = &species_pot[best_clone*species_length];
    memcpy(species, best_clone_buf, species_length);
  }

  free(species_pot);
}

int main()
{
  srand(time(0));

  printf("- WEASEL ALGORITHM - \n");

  char* current_species = (char *)malloc(species_length + 1);

  for(int i = 0; i < species_length; i++)
  {
    int pick = rand() % letter_pot_length;
    current_species[i] = letter_pot[pick];
  }
  current_species[species_length] = '\0';

  int generations = 0;
  while (strcmp(current_species, target) != 0)
  {
    printf("Gen %d: %s\n", generations, current_species);
    clone_and_pick_new_species(current_species);
    generations++;
  }
  printf("Gen %d: %s | COMPLETE!\n", generations, current_species);

  return 0;
}
